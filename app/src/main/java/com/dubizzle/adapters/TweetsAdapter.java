package com.dubizzle.adapters;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dubizzle.dubizzletwitter.R;
import com.dubizzle.interfaces.OnItemClickListener;
import com.dubizzle.interfaces.OnLoadMoreListener;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macadmin on 12/27/17.
 */

public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.ViewHolder> {

    private List<Tweet> tweets = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private OnLoadMoreListener moreListener;
    private Context context;

    public TweetsAdapter(Context context, List<Tweet> tweets, Fragment listener) {
        this.mInflater = LayoutInflater.from(context);
        this.tweets = tweets;
        mClickListener = (OnItemClickListener) listener;
        moreListener = (OnLoadMoreListener) listener;
        this.context = context;
    }

    @Override
    public TweetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.tweet_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TweetsAdapter.ViewHolder holder, int position) {

        if (position == tweets.size() - 1) {
            moreListener.onLoadMore();
        }
        Tweet tweet = tweets.get(position);
        if (tweet.retweeted) {
            Tweet reTweet = tweet.retweetedStatus;
            User user = reTweet.user;
            holder.name.setText(user.name);
            holder.screenName.setText(user.screenName);
            holder.text.setText(tweet.text);
            holder.replyCount.setText(reTweet.inReplyToStatusId + "");
            holder.retweetCount.setText(reTweet.retweetCount + "");
            holder.favoriteCount.setText(reTweet.favoriteCount + "");

            String image = user.profileImageUrlHttps;
            if (!image.isEmpty()) {
                Glide.with(context).load(image).into(holder.profileImage);
            } else {
                Glide.with(context).load("http://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg").into(holder.profileImage);
            }
        } else {
            holder.name.setText(tweet.user.name);
            holder.screenName.setText(tweet.user.screenName);
            holder.text.setText(tweet.text);
            holder.replyCount.setText(tweet.inReplyToStatusId + "");
            holder.retweetCount.setText(tweet.retweetCount + "");
            holder.favoriteCount.setText(tweet.favoriteCount + "");

            String image = tweet.user.profileImageUrlHttps;
            if (!image.isEmpty()) {
                Glide.with(context).load(image).into(holder.profileImage);
            } else {
                Glide.with(context).load("http://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg").into(holder.profileImage);
            }
        }
    }

    public void updateList(List<Tweet> list) {
        tweets = list;
        notifyDataSetChanged();
    }

    public Tweet getItem(int position) {
        return tweets.get(position);
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, screenName, text, replyCount, retweetCount, favoriteCount;
        ImageView profileImage;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            screenName = itemView.findViewById(R.id.screenName);
            text = itemView.findViewById(R.id.text);
            replyCount = itemView.findViewById(R.id.replyCount);
            retweetCount = itemView.findViewById(R.id.retweetCount);
            favoriteCount = itemView.findViewById(R.id.favoriteCount);
            profileImage = itemView.findViewById(R.id.profileImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
