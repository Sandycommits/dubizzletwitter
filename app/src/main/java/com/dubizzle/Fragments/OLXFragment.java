package com.dubizzle.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubizzle.adapters.TweetsAdapter;
import com.dubizzle.dubizzletwitter.R;
import com.dubizzle.interfaces.OnItemClickListener;
import com.dubizzle.interfaces.OnLoadMoreListener;
import com.dubizzle.utils.CommonMethods;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by macadmin on 12/28/17.
 */

public class OLXFragment extends BaseFragment implements OnItemClickListener, OnLoadMoreListener {

    @BindView(R.id.timeLineRecycler)
    RecyclerView timeLineRecycler;
    @BindView(R.id.loader)
    AVLoadingIndicatorView loader;
    private Long lastID = null;
    private List<Tweet> tweets;

    public OLXFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tweets = new ArrayList<>();
        loadTimeline();
    }

    private void loadTimeline() {

        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        StatusesService statusesService = twitterApiClient.getStatusesService();
        loader.setVisibility(View.VISIBLE);
        if (CommonMethods.isOnline(getActivity())) {
            Call<List<Tweet>> call = statusesService.userTimeline(null, getString(R.string.olx_screen_name), 50, null, lastID, null, true, null, true);
            loader.show();
            call.enqueue(new Callback<List<Tweet>>() {
                @Override
                public void success(Result<List<Tweet>> result) {
                    loader.hide();
                    tweets.addAll(result.data);
                    if (tweets != null && tweets.size() > 0) {
                        int size = tweets.size();
                        Tweet lastTweet = tweets.get(size - 1);
                        tweets.remove(size - 1);
                        if (lastID != null) {
                            adapter.updateList(tweets);
                        } else {
                            loadTweets(tweets);
                        }
                        lastID = lastTweet.getId();
                    }
                }

                @Override
                public void failure(TwitterException exception) {
                    loader.hide();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet_connection));
        }
    }

    private void loadTweets(List<Tweet> data) {
        adapter = new TweetsAdapter(getActivity(), data, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        timeLineRecycler.setHasFixedSize(true);
        timeLineRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        timeLineRecycler.setLayoutManager(layoutManager);
        timeLineRecycler.setAdapter(adapter);
    }

    @Override
    public void onLoadMore() {
        loadTimeline();
    }
}
