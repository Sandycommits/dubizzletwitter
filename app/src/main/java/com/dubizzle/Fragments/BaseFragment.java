package com.dubizzle.Fragments;

import android.app.Fragment;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dubizzle.adapters.TweetsAdapter;
import com.dubizzle.dubizzletwitter.R;
import com.dubizzle.interfaces.OnItemClickListener;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;

/**
 * Created by macadmin on 12/28/17.
 */

public class BaseFragment extends Fragment implements OnItemClickListener {


    public TweetsAdapter adapter;

    @Override
    public void onItemClick(int position) {

        Tweet tweet = adapter.getItem(position);
        User user;
        if (tweet.retweeted) {
            user = tweet.retweetedStatus.user;
            tweet = tweet.retweetedStatus;
        } else {
            user = tweet.user;
        }
        String url = "https://twitter.com/" + user.screenName + "/status/" + tweet.idStr;
        boolean wrapInScrollView = true;
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.tweet_details)
                .customView(R.layout.tweet_dialog, wrapInScrollView)
                .positiveText(R.string.close)
                .show();
        View view = dialog.getCustomView();
        WebView webView = view.findViewById(R.id.tweetWebView);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }
        });

    }
}
