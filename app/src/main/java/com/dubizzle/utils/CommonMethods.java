package com.dubizzle.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by macadmin on 12/26/17.
 */

public class CommonMethods {

    private static final String TWITTER = "TWITTER";

    /**
     * Prints log statement
     *
     * @param msg
     */
    public static void getLog(String msg) {
        Log.i(TWITTER, msg);
    }

    /**
     * displays the toast message
     *
     * @param ctx
     * @param msg
     */
    public static void displayToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    /*
    * Display Sample dialogue with "OK" button
    * */
    public static void displayOKDialogue(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
    * Checks whether internet connection is available or not
    * */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
