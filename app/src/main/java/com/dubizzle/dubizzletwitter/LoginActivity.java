package com.dubizzle.dubizzletwitter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dubizzle.utils.CommonMethods;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.loginButton)
    TwitterLoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.twitter_login));
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (session != null) {
            startActivity(new Intent(this, TweetsActivity.class));
            this.finish();
        } else {
            performLogin();
        }
    }

    private void performLogin() {
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                CommonMethods.getLog("Callback success");
                Intent intent = new Intent(LoginActivity.this, TweetsActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                if (CommonMethods.isOnline(LoginActivity.this)) {
                    CommonMethods.displayOKDialogue(LoginActivity.this, getString(R.string.something_went_wrong));
                } else {
                    CommonMethods.displayOKDialogue(LoginActivity.this, getString(R.string.no_internet_connection));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
        CommonMethods.getLog("onActivityResult");
    }
}

