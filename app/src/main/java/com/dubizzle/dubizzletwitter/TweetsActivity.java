package com.dubizzle.dubizzletwitter;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.dubizzle.Fragments.AndroidDevFragment;
import com.dubizzle.Fragments.OLXFragment;
import com.dubizzle.Fragments.TimelineFragment;
import com.twitter.sdk.android.core.TwitterCore;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TweetsActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.logout)
    ImageButton logout;
    @BindView(R.id.toggle)
    Switch toggle;
    @BindView(R.id.language)
    TextView language;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    private FragmentManager fragManager;
    private Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweets);
        ButterKnife.bind(this);
        initialSetup();
    }

    private void initialSetup() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.tweets));
        logout.setVisibility(View.VISIBLE);
        toggle.setVisibility(View.VISIBLE);
        language.setVisibility(View.VISIBLE);
        config = getResources().getConfiguration();
        if (config.locale.getLanguage().equalsIgnoreCase("en")) {
            language.setText(config.locale.getLanguage());
        } else {
            language.setText(getString(R.string.arabic));
        }
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        fragManager = getFragmentManager();
        fragManager.beginTransaction().add(R.id.container, new TimelineFragment()).commit();
        toggle.setOnCheckedChangeListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_timeline:
                fragManager.beginTransaction().replace(R.id.container, new TimelineFragment()).commit();
                break;
            case R.id.action_olx:
                fragManager.beginTransaction().replace(R.id.container, new OLXFragment()).commit();
                break;
            case R.id.action_android:
                fragManager.beginTransaction().replace(R.id.container, new AndroidDevFragment()).commit();
                break;
        }
        return true;
    }

    @OnClick({R.id.logout})
    public void onViewClicks(View view) {
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }


    private void changeLanguage() {
        Locale current = config.locale;
        Locale locale = new Locale(current.getLanguage().equalsIgnoreCase("en") ? "ar" : "en");
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        getSupportActionBar().setTitle(getString(R.string.tweets));
        startActivity(new Intent(this, TweetsActivity.class));
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            changeLanguage();
        } else {
            changeLanguage();
        }
    }
}
