package com.dubizzle.dubizzletwitter;

import android.app.Application;

import com.twitter.sdk.android.core.Twitter;

/**
 * Created by macadmin on 12/26/17.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Twitter.initialize(this);
    }
}
